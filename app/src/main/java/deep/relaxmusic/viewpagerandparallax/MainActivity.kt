package deep.relaxmusic.viewpagerandparallax

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.ViewGroup
import com.ToxicBakery.viewpager.transforms.CubeOutTransformer
import deep.relaxmusic.viewpagerandparallax.adapters.FragmentsPagerAdapter
import deep.relaxmusic.viewpagerandparallax.fragments.RvFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var fragmentPagerAdapter: FragmentsPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createFragmentViewPager()
    }

    fun createFragmentViewPager(){
        fragmentPagerAdapter = FragmentsPagerAdapter(supportFragmentManager)
        fragmentPagerAdapter?.addFrag(RvFragment.newInstance(), "One")
        fragmentPagerAdapter?.addFrag(RvFragment.newInstance(), "Two")
        fragmentPagerAdapter?.addFrag(RvFragment.newInstance(), "Three")
        viewPager.adapter = fragmentPagerAdapter
        viewPager.offscreenPageLimit = 3
        viewPager.setPageTransformer(true, CubeOutTransformer())
    }
}
