package deep.relaxmusic.viewpagerandparallax.fragments

import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.yayandroid.parallaxrecyclerview.sample.ImageRecyclerAdapter
import deep.relaxmusic.viewpagerandparallax.R


class RvFragment : Fragment() {

    var adapter: ImageRecyclerAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_scroll_image, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)
        adapter = ImageRecyclerAdapter(context!!)
        recyclerView.adapter = adapter
    }

    companion object {

        fun newInstance(): RvFragment {
            val fragment = RvFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
