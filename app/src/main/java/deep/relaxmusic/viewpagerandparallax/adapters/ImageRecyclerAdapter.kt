package com.yayandroid.parallaxrecyclerview.sample

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import deep.relaxmusic.viewpagerandparallax.R
import deep.relaxmusic.viewpagerandparallax.parallax_image_view.ScrollParallaxImageView
import deep.relaxmusic.viewpagerandparallax.parallax_image_view.VerticalMovingStyle


class ImageRecyclerAdapter(private val context: Context) : RecyclerView.Adapter<ImageRecyclerAdapter.ViewHolder>() {
    private val inflater: LayoutInflater


    private val imageIds = intArrayOf(
            R.drawable.water_ocean_walking_man,
            R.drawable.water_ocean_walking_man,
            R.drawable.water_ocean_walking_man,
            R.drawable.water_ocean_walking_man,
            R.drawable.water_ocean_walking_man)

    init {
        this.inflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.item_image, viewGroup, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.imageView.setImageResource(imageIds[position % imageIds.size])
        viewHolder.imageView.setParallaxStyles(VerticalMovingStyle()) // or other parallax styles
        Log.v("TestRVLog","onBindViewHolder $position")
    }

    override fun getItemCount(): Int {
        return imageIds.size
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        val imageView: ScrollParallaxImageView

        init {

            imageView = v.findViewById(R.id.backgroundImage) as ScrollParallaxImageView
        }

    }


}
